### 基于 React、Antd、Dnd-kit、的表单设计器

- 核心依赖

  - [ant-design](https://ant.design/index-cn) - 基础组件样式 ui
  - [dnd kit](https://dndkit.com/) -拖拽库
  - [immutability-helper](https://www.npmjs.com/package/immutability-helper) - immutabile 库
  - [tinymce](https://www.tiny.cloud/) - 富文本编辑器

- 里程碑

  - [x] 文本输入
  - [x] 多行文本输入
  - [x] 数字
  - [x] 日期
  - [x] 单选框
  - [ ] 单选框异步加载项
  - [x] 评分
  - [x] 下拉
  - [ ] 下拉异步加载项
  - [x] 逻辑选择
  - [x] 多选
  - [ ] 多选异步加载
  - [x] 富文本
  - [ ] 富文本内附件上传
  - [x] 地址选择
  - [ ] 滑块
  - [ ] 文本
  - [ ] 文字链接
  - [ ] 级联选择
  - [ ] 树选择
  - [ ] 步骤条
  - [ ] 子表单
  - [ ] 按钮
  - [ ] 图片上传组件
  - [ ] 附件上传组件
  - [ ] 表单组件验证规则

### 效果图
![输入图片说明](images/img01.png)
![输入图片说明](images/img02.png)
![输入图片说明](images/img03.png)
![输入图片说明](images/img04.png)