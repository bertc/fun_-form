/* config-overrides.js */
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");
const resolve = (name) => path.join(__dirname, name);
const {
  override,
  addLessLoader,
  adjustStyleLoaders,
  addWebpackPlugin,
} = require("customize-cra");
module.exports = override(
  addLessLoader({
    lessOptions: {
      javascriptEnabled: true,
      cssLoaderOptions: {
        url: false,
      },
    },
  }),
  adjustStyleLoaders(({ use: [, , postcss] }) => {
    const postcssOptions = postcss.options;
    postcss.options = { postcssOptions };
  }),
  addWebpackPlugin(
    new CopyWebpackPlugin({
      patterns: [
        {
          from: resolve("tinyMCE"),
          to: resolve("build/tinyMCE"),
        },
      ],
    })
  )
);
