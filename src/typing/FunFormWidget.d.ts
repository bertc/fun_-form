/// <reference types='moment.d.ts' />

declare type FunOption = { label: string; value?: string };

declare interface DateWidget extends FunFormWidget {
  value?: moment.Moment;
}

declare interface CheckboxWidget extends FunFormWidget {
  options: Array<FunOption>;
  value?: Array<string>;
}

declare interface SwitchWidget extends FunFormWidget {
  value?: string;
  checkedValue: string;
  unCheckedValue: string;
}

declare interface NumberWidget extends FunFormWidget {
  value?: number;
  max?: number;
  min?: number;
}

declare interface RadioWidget extends FunFormWidget {
  options: Array<FunOption>;
  value?: string;
}

declare interface SelectWidget extends FunFormWidget {
  options: Array<FunOption>;
  value?: string | Array<string>;
  allowClear: boolean;
  selectMode?: "multiple";
}

declare interface TextWidget extends FunFormWidget {
  value?: string;
}

declare interface TextareaWidget extends FunFormWidget {
  value?: string;
  maxLength?: number;
  showCount?: boolean;
}

declare interface RichTextWidget extends FunFormWidget {
  value?: string;
}

declare interface RateWidget extends FunFormWidget {
  value: number;
  count: number;
}

declare interface AddressWidget extends FunFormWidget {
  allowClear: boolean;
  addressMode: AddressMode;
  country: string;
  province: string;
  city: string;
  district: string;
  detail: string;
}
