declare enum WidgetType {
  Text = "text",
  Textarea = "textarea",
  Number = "number",
  Radio = "radio",
  Date = "date",
  CheckBox = "checkbox",
  Select = "select",
  Switch = "switch",
  RichText = "richText",
  Rate = "rate",
  Address = "address",
}

declare enum FunFormElement {
  Cagetroy = "cagtegory",
  Widget = "widget",
  Group = "group",
  Card = "card",
  Button = "button",
}

declare enum AddressMode {
  /**国家 */
  c = "c",
  /**省市 */
  p_c = "pc",
  /**省市区县 */
  p_c_d = "pcd",
  /**省市区县详情 */
  p_c_d_d = "pcdd",
}

declare interface FunFormWidget {
  uuid: string;
  category: WidgetCategory;
  type: FunFormElement;
  label: string;
  hidelabel: boolean;
  placeholder?: string;
  labelWidth?: number;
  isRequired: boolean;
  name?: string;
  value?: any;
}

declare interface FunFormCardColumn {
  uuid: string;
  colSpan: number;
  category?: WidgetCategory;
  widget?: FunFormWidget;
}

declare interface FunFormCardRow {
  uuid: string;
  columns: Array<FunFormCardColumn>;
}

declare interface FunFormCard {
  title: string;
  type: FunFormElement;
  uuid: string;
  isHide: boolean;
  border: 1 | 2;
  labelWidths: number[];
  mode: "vertical" | "horizontal";
  rows: Array<FunFormCardRow>;
}

declare interface FunFormGroup {
  title: string;
  /** 1 无边框 2 有边框 3 tabs 页 */
  border: 1 | 2 | 3;
  type: FunFormElement;
  uuid: string;
  isHide: boolean;
  cards: Array<FunFormCard>;
}

declare interface FunFormButton {
  title: string;
  uuid: string;
  type: FunFormElement;
  isSystemBtn: boolean;
  isHieghtLight?: boolean;
}

declare interface FunFormSchema {
  title: string;
  groups: Array<FunFormGroup>;
  buttonGroups: Array<FunFormButton>;
}

declare interface WidgetCategory {
  icon?: string;
  name: string;
  type: WidgetType;
  /** 弱 行占位 */
  defaultFull?: boolean;
  /** 强 行占位 */
  allwaysFull?: boolean;
}

declare interface ActivedElementAt {
  groupIndex: number;
  cardIndex: number;
  rowIndex: number;
  colIndex: number;
}

declare type ActivedElemnetType<ElementType> = {
  element: ElementType;
  position: ActivedElementAt;
};

declare interface FunForm {
  instance: FunFormSchema;
  updateInstance: (instance: FunFormSchema) => void;
  activedWidgetCategory?: WidgetCategory;
  setActivedWidgetCategory: (w?: WidgetCategory) => void;
  activedWidget?: FunFormWidget;
  setActivedWidget: (widget?: FunFormWidget) => void;
  activedElement?: ActivedElemnetType<
    FunFormWidget | FunFormGroup | FunFormCard | FunFormButton
  >;
  setActivedElement: (
    element?: ActivedElemnetType<
      FunFormWidget | FunFormGroup | FunFormCard | FunFormButton
    >
  ) => void;
}
