import React from "react";
import { render } from "react-dom";
import "./index.less";
import FunForm from "./FunForm";

render(<FunForm />, document.querySelector("#root"));
