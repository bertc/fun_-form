import { Slider } from "antd";
import React, { memo } from "react";
import styles from "../index.module.less";

export default memo(
  ({
    info,
    onChange,
  }: {
    info: FunFormCard;
    onChange(val: number, idx: number): void;
  }) => {
    return (
      <div className={styles.ruler}>
        {info.labelWidths.map((r, i) => {
          return (
            <div className={styles.kd} key={info.uuid + "_rule_" + i}>
              标题宽度：
              <Slider
                className={styles.steps}
                min={50}
                max={220}
                value={r}
                onChange={(val) => {
                  onChange(val, i);
                }}
              />
            </div>
          );
        })}
      </div>
    );
  }
);
