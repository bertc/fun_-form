import React from "react";
import WrapItem, { WarpAction } from "./toolWrap";
import DefaultDropBlock from "./defaultDropBlock";
import DropItem from "./dropItem";
import DragItem from "./dragItem";
import styles from "../index.module.less";
import { FunFormElement } from "../../core";

type RowRenderProps = {
  info: FunFormCardRow;
  labelWidths: number[];
  couldDelete: boolean;
  onUpdateRow(row: FunFormCardRow): void;
  onInsertRow(type: 1 | 2): void;
  onDeleteRow(): void;
  onInsertCol(colIndex: number, type: 1 | 2): void;
  onDeleteCol(colIndex: number): void;
  onMergeColumn(colIndex: number): void;
  onMergeCancel(colIndex: number): void;
  onCreateByCategory(colIndex: number, value: WidgetCategory): void;
  onMoveColunmWidget(targetId: string, moveToId: string): void;
  onMoveWidgetIn(widget: FunFormWidget, colId: string, at: number): void;
  onSelect(col: number): void;
  onDeleteWidget(col: number): void;
  mode: "horizontal" | "vertical";
};

const RowRender = ({
  info,
  labelWidths,
  couldDelete,
  onInsertRow,
  onDeleteCol,
  onDeleteRow,
  onCreateByCategory,
  onMergeCancel,
  onMergeColumn,
  onInsertCol,
  onUpdateRow,
  onMoveColunmWidget,
  onMoveWidgetIn,
  onSelect,
  onDeleteWidget,
  mode,
}: RowRenderProps) => {
  if (info.columns.length === 0) {
    return (
      <DefaultDropBlock
        labelWidths={labelWidths}
        onTrigger={(type) => {
          switch (type) {
            case WarpAction.insertBefore:
              onInsertRow(1);
              break;
            case WarpAction.insertAfter:
              onInsertRow(2);
              break;
            case WarpAction.deleteRow:
              onDeleteRow();
              break;
            case WarpAction.insertLeft:
              onInsertCol(0, 1);
              break;
            case WarpAction.insertRight:
              onInsertCol(0, 2);
              break;
            default:
          }
        }}
        couldDelete={couldDelete}
        uuid={info.uuid}
        updateColunm={(columns) => {
          const row = { ...info };
          row.columns = columns;
          onUpdateRow(row);
        }}
        onWidgetMoveIn={onMoveWidgetIn}
      ></DefaultDropBlock>
    );
  }
  return (
    <div className={styles.rowBlock}>
      {info.columns.map((item, g, row) => {
        const actions = [WarpAction.deleteRow];
        if (!(item.category && item.category.allwaysFull)) {
          actions.push(WarpAction.insertLeft);
          actions.push(WarpAction.insertRight);
        }
        if (labelWidths.length !== 1) actions.push(WarpAction.deleteCol);
        if (item.colSpan > 1 && !item.category?.allwaysFull) {
          actions.push(WarpAction.mergeCancel);
        }
        if (row[g + 1] && !row[g + 1].category) {
          actions.push(WarpAction.merge);
        }
        let offset = 0;
        for (let i = 0; i < g; i++) {
          offset += row[i].colSpan - 1;
        }

        return (
          <WrapItem
            key={item.uuid}
            style={{ flex: item.colSpan }}
            onTrigger={(type) => {
              switch (type) {
                case WarpAction.insertBefore:
                  onInsertRow(1);
                  break;
                case WarpAction.insertAfter:
                  onInsertRow(2);
                  break;
                case WarpAction.deleteRow:
                  onDeleteRow();
                  break;
                case WarpAction.insertLeft:
                  onInsertCol(g, 1);
                  break;
                case WarpAction.insertRight:
                  onInsertCol(g, 2);
                  break;
                case WarpAction.deleteCol: // 删除列
                  onDeleteCol(g);
                  break;
                case WarpAction.merge: // 删除列
                  onMergeColumn(g);
                  break;
                case WarpAction.mergeCancel: // 删除列
                  onMergeCancel(g);
                  break;
                default:
              }
            }}
            actions={actions}
          >
            {!item.category && (
              <DropItem
                id={item.uuid}
                validAf
                onDrop={(active) => {
                  if (!active) return;
                  let { name, value, colId } = active.data.current as {
                    name: FunFormElement;
                    value: WidgetCategory | FunFormWidget;
                    colId: string;
                  };
                  if (name === FunFormElement.Cagetroy) {
                    const { allwaysFull } = value as WidgetCategory;
                    if (info.columns.length !== 0 && allwaysFull) return;
                    else {
                      onCreateByCategory(g, value as WidgetCategory);
                    }
                  } else if (name === FunFormElement.Widget) {
                    onMoveColunmWidget(colId, item.uuid);
                  }
                }}
              ></DropItem>
            )}
            {item.category && (
              <DragItem
                mode={mode}
                widget={item.widget as FunFormWidget}
                labelWidth={labelWidths[g + offset]} // 处理label 命中项
                colId={item.uuid}
                onSelect={() => {
                  onSelect(g);
                }}
                onDelete={() => {
                  onDeleteWidget(g);
                }}
              ></DragItem>
            )}
          </WrapItem>
        );
      })}
    </div>
  );
};

export default RowRender;
