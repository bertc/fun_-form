import React, { PropsWithChildren } from "react";
import { Dropdown, Menu } from "antd";
import styles from "../index.module.less";

export enum WarpAction {
  insertBefore = 1,
  insertAfter = 2,
  insertLeft = 3,
  insertRight = 4,
  merge = 5,
  mergeCancel = 6,
  deleteRow = 7,
  deleteCol = 8,
}

const ToolsWrap = ({
  children,
  actions,
  onTrigger,
  style,
}: PropsWithChildren<{
  actions: Array<WarpAction>;
  onTrigger(type: WarpAction): void;
  style?: React.CSSProperties;
}>) => {
  return (
    <Dropdown
      trigger={["contextMenu"]}
      overlay={
        <Menu>
          <Menu.SubMenu title="插入">
            <Menu.Item
              onClick={() => onTrigger(WarpAction.insertBefore)}
              key={WarpAction.insertBefore}
            >
              向上插入行
            </Menu.Item>
            <Menu.Item
              onClick={() => onTrigger(WarpAction.insertAfter)}
              key={WarpAction.insertAfter}
            >
              向下插入行
            </Menu.Item>
            <Menu.Item
              key={WarpAction.insertRight}
              disabled={!actions.includes(WarpAction.insertRight)}
              onClick={() => onTrigger(WarpAction.insertRight)}
            >
              右侧插入列
            </Menu.Item>
            <Menu.Item
              key={WarpAction.insertLeft}
              disabled={!actions.includes(WarpAction.insertLeft)}
              onClick={() => onTrigger(WarpAction.insertLeft)}
            >
              左侧插入列
            </Menu.Item>
          </Menu.SubMenu>
          <Menu.Divider></Menu.Divider>
          <Menu.Item
            key={WarpAction.merge}
            disabled={!actions.includes(WarpAction.merge)}
            onClick={() => onTrigger(WarpAction.merge)}
          >
            向右合并
          </Menu.Item>
          <Menu.Item
            key={WarpAction.mergeCancel}
            disabled={!actions.includes(WarpAction.mergeCancel)}
            onClick={() => onTrigger(WarpAction.mergeCancel)}
          >
            取消合并
          </Menu.Item>
          <Menu.Divider></Menu.Divider>
          <Menu.Item
            key={WarpAction.deleteRow}
            disabled={!actions.includes(WarpAction.deleteRow)}
            onClick={() => onTrigger(WarpAction.deleteRow)}
          >
            删除行
          </Menu.Item>
          <Menu.Item
            key={WarpAction.deleteCol}
            disabled={!actions.includes(WarpAction.deleteCol)}
            onClick={() => onTrigger(WarpAction.deleteCol)}
          >
            删除列
          </Menu.Item>
        </Menu>
      }
      arrow
    >
      <div
        style={style}
        className={styles.item}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        {children}
      </div>
    </Dropdown>
  );
};

export default ToolsWrap;
