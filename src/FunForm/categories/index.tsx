import React, { memo, useContext } from "react";
import Scrollbars from "react-custom-scrollbars";
import { useDraggable, useDndMonitor } from "@dnd-kit/core";
import classNames from "classnames";
import DndKitFormContext, { FunFormElement, WidgetType } from "../core";
import { Collapse } from "antd";
import styles from "./index.module.less";

const categories: Array<WidgetCategory> = [
  {
    icon: "text",
    name: "文本",
    type: WidgetType.Text,
  },
  {
    icon: "textarea",
    name: "多行文本",
    type: WidgetType.Textarea,
    defaultFull: true,
  },
  {
    icon: "number",
    name: "数字",
    type: WidgetType.Number,
  },
  {
    icon: "date",
    name: "日期",
    type: WidgetType.Date,
  },
  {
    icon: "radio",
    name: "单选框",
    type: WidgetType.Radio,
  },
  {
    icon: "rate",
    name: "评分",
    type: WidgetType.Rate,
  },
  {
    icon: "select",
    name: "下拉框",
    type: WidgetType.Select,
  },
  {
    icon: "switch",
    name: "逻辑选择",
    type: WidgetType.Switch,
  },
  {
    icon: "checkbox",
    name: "多选框",
    type: WidgetType.CheckBox,
  },
  {
    icon: "richText",
    name: "富文本",
    type: WidgetType.RichText,
    allwaysFull: true,
  },
  {
    icon: "address",
    name: "地址",
    type: WidgetType.Address,
    defaultFull: true,
  },
];

const { Panel } = Collapse;

export const DraggableItem = memo(
  ({
    onClick,
    isOverLayer,
    ...value
  }: WidgetCategory & {
    onClick?(): void;
    isOverLayer: boolean;
  }) => {
    const { attributes, isDragging, listeners, setNodeRef } = useDraggable({
      id: "category_" + value.type,
      data: {
        name: FunFormElement.Cagetroy,
        value,
      },
    });

    return (
      <div
        className={classNames(
          styles.category,
          isOverLayer && styles.isOverLayer,
          isDragging && styles.dragging
        )}
      >
        <div
          className={styles.target}
          ref={setNodeRef}
          {...attributes}
          {...listeners}
          onClick={onClick}
        >
          <b>{value.icon}</b>
          <span>{value.name}</span>
        </div>
      </div>
    );
  }
);

const CagetroyNav = () => {
  const { setActivedWidgetCategory, setActivedWidget } =
    useContext(DndKitFormContext);

  useDndMonitor({
    onDragStart({ active }) {
      const current = active.data.current as {
        name: FunFormElement;
        value: WidgetCategory;
      };
      if (current && current.name === FunFormElement.Cagetroy) {
        setActivedWidgetCategory(current.value);
      }
    },
    onDragEnd() {
      setActivedWidgetCategory(undefined);
      setActivedWidget(undefined);
    },
  });
  return (
    <Scrollbars>
      <Collapse bordered={false} defaultActiveKey={["1"]}>
        <Panel header="标准组件" key="1" style={{ padding: 0 }}>
          <div className={styles.cagetroies}>
            {categories.map((item) => {
              return (
                <DraggableItem
                  isOverLayer={false}
                  key={item.name + "_" + item.type}
                  onClick={() => {}}
                  {...item}
                />
              );
            })}
          </div>
        </Panel>
      </Collapse>
    </Scrollbars>
  );
};

export default CagetroyNav;
