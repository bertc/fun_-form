import React, { memo, useCallback, useContext, useEffect } from "react";
import update from "immutability-helper";
import { Form, Input, Checkbox, Select } from "antd";
import FunFormCtx from "../core";

const { Item: FormItem } = Form;

const Card = memo((props: ActivedElemnetType<FunFormCard>) => {
  const { instance, updateInstance } = useContext(FunFormCtx);

  const [form] = Form.useForm<FunFormCard>();

  const { element, position } = props;

  useEffect(() => {
    form.setFieldsValue(element as FunFormCard);
  }, [element, form]);

  const handleChange = useCallback(() => {
    form.validateFields().then((value: FunFormCard) => {
      updateInstance(
        update(instance, {
          groups: {
            [position.groupIndex]: {
              cards: {
                [position.cardIndex]: {
                  $apply(old) {
                    return { ...old, ...value };
                  },
                },
              },
            },
          },
        })
      );
    });
  }, [instance, position, form, updateInstance]);

  return (
    <Form
      form={form}
      layout="vertical"
      initialValues={element}
      onChange={handleChange}
      onFinish={handleChange}
    >
      <FormItem
        label="分组名称"
        required
        name="title"
        rules={[
          {
            required: true,
            message: "请输入分组名称",
          },
        ]}
      >
        <Input></Input>
      </FormItem>
      <FormItem label="表单布局样式" name="mode">
        <Select
          onChange={(v) => {
            form.setFieldsValue({ mode: v });
            form.submit();
          }}
        >
          <Select.Option value="horizontal">horizontal</Select.Option>
          <Select.Option value="vertical">vertical</Select.Option>
        </Select>
      </FormItem>
      <FormItem label="分区样式" name="border">
        <Select
          onChange={(value) => {
            form.setFieldsValue({ border: value });
            form.submit();
          }}
        >
          <Select.Option value={1}>无边框</Select.Option>
          <Select.Option value={2}>有边框</Select.Option>
        </Select>
      </FormItem>
      <FormItem label="是否隐藏" valuePropName="checked" name="isHide">
        <Checkbox
          onChange={({ target: { checked } }) => {
            form.setFieldsValue({ isHide: checked });
            form.submit();
          }}
        ></Checkbox>
      </FormItem>
    </Form>
  );
});

export default Card;
