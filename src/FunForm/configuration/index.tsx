import React, { useContext, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import { Tabs } from "antd";
import FunFormCtx, { FunFormElement } from "../core";
import GroupItem from "./group";
import CardItem from "./card";
import WidgetItem from "./widgets";
import ButtonCfg from "./button";

const { TabPane } = Tabs;

const Configurations = () => {
  const { activedElement } = useContext(FunFormCtx);
  const [activeKey, setActiveKey] = useState<string>("1");
  return (
    <Scrollbars>
      <div style={{ padding: "0 15px" }}>
        <Tabs
          activeKey={activeKey}
          onChange={(key) => {
            setActiveKey(key);
          }}
        >
          <TabPane key="1" tab="组件属性">
            {activedElement?.element &&
              {
                [FunFormElement.Group]: (
                  <GroupItem
                    {...(activedElement as ActivedElemnetType<FunFormGroup>)}
                  />
                ),
                [FunFormElement.Card]: (
                  <CardItem
                    {...(activedElement as ActivedElemnetType<FunFormCard>)}
                  />
                ),
                [FunFormElement.Widget]: (
                  <WidgetItem
                    {...(activedElement as ActivedElemnetType<FunFormWidget>)}
                  />
                ),
                [FunFormElement.Cagetroy]: <></>,
                [FunFormElement.Button]: (
                  <ButtonCfg
                    {...(activedElement as ActivedElemnetType<FunFormButton>)}
                  />
                ),
              }[activedElement?.element.type]}

            {!activedElement?.element && (
              <div
                style={{
                  textAlign: "center",
                  color: "#727272",
                }}
              >
                请先选择一个组件
              </div>
            )}
          </TabPane>
          <TabPane key="2" tab="表单属性"></TabPane>
        </Tabs>
      </div>
    </Scrollbars>
  );
};

export default Configurations;
