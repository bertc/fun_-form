import React, { useContext, useEffect } from "react";
import update from "immutability-helper";
import { Form, Input, Checkbox, Alert } from "antd";
import DndFormCtx from "../core";

const { Item: FormItem } = Form;

const ButtonConfig = (props: ActivedElemnetType<FunFormButton>) => {
  const { element, position } = props;
  const { instance, updateInstance } = useContext(DndFormCtx);

  const [form] = Form.useForm<FunFormButton>();

  useEffect(() => {
    form.setFieldsValue({ ...element });
  }, [element, form]);

  return (
    <Form
      form={form}
      layout="vertical"
      initialValues={{ ...element }}
      onChange={() => {
        form.validateFields().then((value: FunFormButton) => {
          updateInstance(
            update(instance, {
              buttonGroups: {
                [position.groupIndex]: {
                  $apply(old) {
                    return { ...old, ...value };
                  },
                },
              },
            })
          );
        });
      }}
    >
      <FormItem
        label="按钮名称"
        required
        name="title"
        rules={[
          {
            required: true,
            message: "请输入分区名称",
          },
        ]}
      >
        <Input></Input>
      </FormItem>
      {element.isSystemBtn && (
        <Alert type="warning" message="系统默认菜单不支持编辑"></Alert>
      )}
      <FormItem label="是否高亮" valuePropName="checked" name="isHieghtLight">
        <Checkbox disabled={element.isSystemBtn}></Checkbox>
      </FormItem>
    </Form>
  );
};

export default ButtonConfig;
