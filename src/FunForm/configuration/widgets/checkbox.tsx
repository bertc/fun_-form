import React, { useState } from "react";
import { Form, FormInstance, Select } from "antd";
import WidegetWrap from "./widgetWrap";
import Options from "./options";

const { Item: FormItem } = Form;

const CheckboxRender = (props: ActivedElemnetType<CheckboxWidget>) => {
  const [options, setOptions] = useState<Array<FunOption> | undefined>(
    props.element.options
  );
  return (
    <WidegetWrap {...props}>
      {(form: FormInstance) => {
        return (
          <React.Fragment>
            <FormItem noStyle name="options">
              <Options
                value={options}
                onChange={(value = []) => {
                  setOptions(value);
                  const defaultValue = form.getFieldValue("value");
                  if (
                    defaultValue &&
                    value.findIndex(({ value: v }) => v === defaultValue) > -1
                  ) {
                    form.setFieldsValue({ value: null });
                  }
                  form.setFieldsValue({
                    options: value,
                  });
                  form.submit();
                }}
              />
            </FormItem>
            <FormItem label="缺省" name="value">
              <Select
                mode="multiple"
                allowClear
                placeholder="设置缺省值"
                onChange={(value) => {
                  form.setFieldsValue({ value: value });
                  form.submit();
                }}
              >
                {options
                  ?.filter((x) => x.label && x.value)
                  .map((opt, i) => (
                    <Select.Option key={opt.value + "_" + i} value={opt.value}>
                      {opt.label}
                    </Select.Option>
                  ))}
              </Select>
            </FormItem>
          </React.Fragment>
        );
      }}
    </WidegetWrap>
  );
};

export default CheckboxRender;
