import React from "react";
import { Form, Input, Select, Checkbox } from "antd";
import { AddressMode } from "../../core";
import WidegetWrap from "./widgetWrap";
import { FormInstance } from "rc-field-form";
const { Item: FormItem } = Form;

const AddressCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  return (
    <WidegetWrap {...props} withBase={false}>
      {(form: FormInstance) => {
        return (
          <React.Fragment>
            <FormItem
              label="标题"
              name="label"
              required
              rules={[
                {
                  required: true,
                  message: "请输入标题",
                },
              ]}
            >
              <Input placeholder="请输入标题" />
            </FormItem>
            <FormItem
              label="是否必填"
              valuePropName="checked"
              name="isRequired"
            >
              <Checkbox
                onChange={({ target: { checked } }) => {
                  form.setFieldsValue({ isRequired: checked });
                  form.submit();
                }}
              ></Checkbox>
            </FormItem>
            <FormItem label="模式" name="addressMode">
              <Select
                onChange={(value) => {
                  form.setFieldsValue({ addressMode: value });
                  form.submit();
                }}
              >
                <Select.Option value={AddressMode.c}>国家</Select.Option>
                <Select.Option value={AddressMode.p_c}>省市</Select.Option>
                <Select.Option value={AddressMode.p_c_d}>
                  省市区/县
                </Select.Option>
                <Select.Option value={AddressMode.p_c_d_d}>
                  省市区/县/详情
                </Select.Option>
              </Select>
            </FormItem>
            <FormItem shouldUpdate={(p, n) => p.addressMode !== n.addressMode}>
              {({ getFieldValue }) => {
                const mode = getFieldValue("addressMode") as AddressMode;

                return (
                  <React.Fragment>
                    {mode === AddressMode.c && (
                      <FormItem
                        label="绑定国家收集字段"
                        name="country"
                        required
                        rules={[
                          {
                            required: true,
                            message: "请绑定'国家'收集字段",
                          },
                        ]}
                      >
                        <Input></Input>
                      </FormItem>
                    )}
                    {mode !== AddressMode.c && (
                      <FormItem
                        label="绑定省份收集字段"
                        name="province"
                        required
                        tooltip="省份行政区划代码相关联"
                        rules={[
                          {
                            required: true,
                            message: "请绑定'省份'收集字段",
                          },
                        ]}
                      >
                        <Input></Input>
                      </FormItem>
                    )}
                    {mode !== AddressMode.c && (
                      <FormItem
                        label="绑定城市收集字段"
                        tooltip="城市行政区划代码相关联"
                        name="city"
                        required
                        rules={[
                          {
                            required: true,
                            message: "请绑定'城市'收集字段",
                          },
                        ]}
                      >
                        <Input></Input>
                      </FormItem>
                    )}
                    {[AddressMode.p_c_d, AddressMode.p_c_d_d].includes(
                      mode
                    ) && (
                      <FormItem
                        label="绑定区/县收集字段"
                        name="district"
                        required
                        tooltip="区/县行政区划代码相关联"
                        rules={[
                          {
                            required: true,
                            message: "请绑定'区/县'收集字段",
                          },
                        ]}
                      >
                        <Input></Input>
                      </FormItem>
                    )}
                    {mode === AddressMode.p_c_d_d && (
                      <FormItem
                        label="绑定详情收集字段"
                        name="detail"
                        required
                        rules={[
                          {
                            required: true,
                            message: "请绑定'详情'收集字段",
                          },
                        ]}
                      >
                        <Input></Input>
                      </FormItem>
                    )}
                  </React.Fragment>
                );
              }}
            </FormItem>
          </React.Fragment>
        );
      }}
    </WidegetWrap>
  );
};

export default AddressCfg;
