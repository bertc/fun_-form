import React from "react";
import { Form, Input } from "antd";
import WidegetWrap from "./widgetWrap";
const { Item: FormItem } = Form;

const TextCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  return (
    <WidegetWrap {...props}>
      <FormItem label="缺省" name="value">
        <Input placeholder="输入缺省值" />
      </FormItem>
    </WidegetWrap>
  );
};

export default TextCfg;
