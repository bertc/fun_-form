import React, { useMemo } from "react";
import TextC from "./text";
import TextareaC from "./textarea";
import NumberC from "./number";
import DateC from "./date";
import RadioC from "./radio";
import RateC from "./rate";
import CheckboxC from "./checkbox";
import SelectC from "./select";
import SwitchC from "./switch";
import RichText from "./richText";
import AddressCfg from "./address";
import { WidgetType } from "../../core";

const WidgetCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  const { element } = props;
  const type = useMemo(() => element.category.type, [element]);
  const Component = {
    [WidgetType.Text]: TextC,
    [WidgetType.Number]: NumberC,
    [WidgetType.Radio]: RadioC,
    [WidgetType.Date]: DateC,
    [WidgetType.CheckBox]: CheckboxC,
    [WidgetType.Select]: SelectC,
    [WidgetType.Switch]: SwitchC,
    [WidgetType.RichText]: RichText,
    [WidgetType.Rate]: RateC,
    [WidgetType.Address]: AddressCfg,
    [WidgetType.Textarea]: TextareaC,
  }[type];

  return <Component {...(props as any)}></Component>;
};

export default WidgetCfg;
