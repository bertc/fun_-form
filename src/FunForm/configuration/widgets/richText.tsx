import React from "react";
import { Form, Input } from "antd";
import WidegetWrap from "./widgetWrap";
const { Item: FormItem } = Form;

const RichTextCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  return (
    <WidegetWrap {...props}>
      <FormItem label="缺省" name="value">
        <Input placeholder="设置缺省值" />
      </FormItem>
    </WidegetWrap>
  );
};

export default RichTextCfg;
