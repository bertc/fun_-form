import React from "react";
import { Form, Input, InputNumber, Checkbox } from "antd";
import WidegetWrap from "./widgetWrap";
import { FormInstance } from "rc-field-form";
const { Item: FormItem } = Form;

const TextareaCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  return (
    <WidegetWrap {...props}>
      {(form: FormInstance) => {
        return (
          <React.Fragment>
            <FormItem label="缺省" name="value">
              <Input placeholder="输入缺省值" />
            </FormItem>
            <FormItem label="长度限制" name="maxLength">
              <InputNumber style={{ width: "100%" }} />
            </FormItem>
            <FormItem label="显示计数" name="showCount" valuePropName="checked">
              <Checkbox
                onChange={({ target: { checked } }) => {
                  const checkedValue = form.getFieldValue("showCount");
                  form.setFieldsValue({
                    showCount: checked ? checkedValue : undefined,
                  });
                  form.submit();
                }}
              ></Checkbox>
            </FormItem>
          </React.Fragment>
        );
      }}
    </WidegetWrap>
  );
};

export default TextareaCfg;
