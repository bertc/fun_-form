import React, { useMemo, useState } from "react";
import { Form, FormInstance, Select, Checkbox } from "antd";
import WidegetWrap from "./widgetWrap";
import Options from "./options";

const { Item: FormItem } = Form;

const SelectCfg = (props: ActivedElemnetType<SelectWidget>) => {
  const [options, setOptions] = useState<Array<FunOption> | undefined>(
    props.element.options
  );
  const [selectMode, setSelectMode] = useState<1 | 2>(
    props.element.selectMode === "multiple" ? 2 : 1
  );
  const hodleList = useMemo(
    () => options?.filter((x) => x.label && x.value) || [],
    [options]
  );
  return (
    <WidegetWrap {...props}>
      {(form: FormInstance) => {
        return (
          <>
            <FormItem label="模式" name="selectMode" valuePropName="label">
              <Select
                value={selectMode}
                onChange={(value: 1 | 2) => {
                  setSelectMode(value);
                  if (value === 1) {
                    form.setFieldsValue({
                      value: undefined,
                      selectMode: undefined,
                    });
                  } else if (value === 2) {
                    form.setFieldsValue({
                      value: [],
                      selectMode: "multiple",
                    });
                  }
                  form.submit();
                }}
              >
                <Select.Option value={1} label="单选">
                  单选
                </Select.Option>
                <Select.Option value={2} label="多选">
                  多选
                </Select.Option>
              </Select>
            </FormItem>
            <FormItem noStyle name="options">
              <Options
                value={options}
                onChange={(value = []) => {
                  setOptions(value);
                  const defaultValue = form.getFieldValue("value");
                  if (defaultValue) {
                    if (Array.isArray(defaultValue)) {
                      form.setFieldsValue({ value: [] });
                    } else {
                      if (
                        value.findIndex(({ value: v }) => v === defaultValue) >
                        -1
                      ) {
                        form.setFieldsValue({ value: null });
                      }
                    }
                  }
                  form.setFieldsValue({
                    options: value,
                  });
                  form.submit();
                }}
              />
            </FormItem>
            <FormItem
              label="清除按钮"
              name="allowClear"
              valuePropName="checked"
            >
              <Checkbox
                onChange={({ target: { checked } }) => {
                  form.setFieldsValue({ allowClear: checked });
                  form.submit();
                }}
              ></Checkbox>
            </FormItem>
            <FormItem label="缺省" name="value">
              <Select
                placeholder="设置缺省值"
                mode={form.getFieldValue("selectMode")}
                allowClear
                onChange={(value) => {
                  form.setFieldsValue({ value });
                  form.submit();
                }}
              >
                {hodleList.map((opt, i) => (
                  <Select.Option key={opt.value + "_" + i} value={opt.value}>
                    {opt.label}
                  </Select.Option>
                ))}
              </Select>
            </FormItem>
          </>
        );
      }}
    </WidegetWrap>
  );
};

export default SelectCfg;
