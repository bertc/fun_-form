import React from "react";
import { DatePicker, Form, FormInstance } from "antd";
import WidegetWrap from "./widgetWrap";

const { Item: FormItem } = Form;

const DateCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  return (
    <WidegetWrap {...props}>
      {(form: FormInstance) => {
        return (
          <React.Fragment>
            <FormItem label="缺省" name="value">
              <DatePicker
                style={{ width: "100%" }}
                onChange={(value: unknown) => {
                  form.setFieldsValue({ value: value });
                  form.submit();
                }}
                placeholder="设置缺省值"
              ></DatePicker>
            </FormItem>
          </React.Fragment>
        );
      }}
    </WidegetWrap>
  );
};

export default DateCfg;
