import React from "react";
import { Form, InputNumber } from "antd";
import WidegetWrap from "./widgetWrap";
const { Item: FormItem } = Form;

const RateCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  return (
    <WidegetWrap {...props}>
      <FormItem label="总长度" name="count">
        <InputNumber min={1} />
      </FormItem>
      <FormItem label="缺省" name="value">
        <InputNumber />
      </FormItem>
    </WidegetWrap>
  );
};

export default RateCfg;
