import React, { useRef, useState } from "react";
import { Input, Row, Col } from "antd";
import update from "immutability-helper";
import { MinusCircleOutlined } from "@ant-design/icons";
import styles from "./index.module.less";

const InputWrap = (props: { value?: string; onChange(val?: string): void }) => {
  const { value, onChange } = props;
  const [v, setV] = useState(value);
  const ipt = useRef<any>(null);

  return (
    <Input
      defaultValue={v}
      ref={ipt}
      onChange={() => {
        setV(ipt.current?.input.value);
      }}
      onBlur={() => {
        onChange(ipt.current?.input.value);
      }}
    ></Input>
  );
};

const OptionsCfg = ({
  value = [],
  onChange,
}: {
  value?: Array<FunOption>;
  onChange?(newValue?: Array<FunOption>): void;
}) => {
  return (
    <div className={styles.options}>
      <div className={styles.title}>选项设置：</div>
      <Row className={styles.item}>
        <Col span={10}>键名</Col>
        <Col span={10} offset={1}>
          值：
        </Col>
      </Row>
      {value?.map((option, i) => {
        return (
          <Row key={`${option}_${i}`} className={styles.item}>
            <Col span={10}>
              <InputWrap
                value={option.label}
                onChange={(v = "") => {
                  onChange &&
                    onChange(
                      update(value, {
                        [i]: {
                          label: {
                            $set: v,
                          },
                        },
                      })
                    );
                }}
              />
            </Col>
            <Col span={10} offset={1}>
              <InputWrap
                value={option.value}
                onChange={(v) => {
                  onChange &&
                    onChange(
                      update(value, {
                        [i]: {
                          value: {
                            $set: v,
                          },
                        },
                      })
                    );
                }}
              />
            </Col>
            <Col span={2} offset={1}>
              <MinusCircleOutlined
                title="删除"
                onClick={() => {
                  onChange &&
                    onChange(
                      update(value, {
                        $splice: [[i, 1]],
                      })
                    );
                }}
                style={{ color: "red", cursor: "pointer" }}
              />
            </Col>
          </Row>
        );
      })}
      <div className={styles.btn}>
        <span
          onClick={() => {
            onChange &&
              onChange(
                update(value, {
                  $push: [
                    {
                      label: "选项_" + (value.length + 1),
                      value: "值_" + (value.length + 1),
                    },
                  ],
                })
              );
          }}
        >
          新增选项
        </span>
      </div>
    </div>
  );
};

export default OptionsCfg;
