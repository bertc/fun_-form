import React from "react";
import { Form, Checkbox, FormInstance, Input } from "antd";
import WidegetWrap from "./widgetWrap";
const { Item: FormItem } = Form;

const SwitchCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  return (
    <WidegetWrap {...props}>
      {(form: FormInstance) => {
        return (
          <>
            <FormItem
              label="选中值"
              name="checkedValue"
              required
              rules={[{ required: true, message: "设置选中值" }]}
            >
              <Input />
            </FormItem>
            <FormItem
              label="未选中值"
              name="unCheckedValue"
              required
              rules={[{ required: true, message: "设置未选中值" }]}
            >
              <Input />
            </FormItem>
            <FormItem label="默认选中" name="value" valuePropName="checked">
              <Checkbox
                onChange={({ target: { checked } }) => {
                  const checkedValue = form.getFieldValue("checkedValue");
                  form.setFieldsValue({
                    value: checked ? checkedValue : undefined,
                  });
                  form.submit();
                }}
              ></Checkbox>
            </FormItem>
          </>
        );
      }}
    </WidegetWrap>
  );
};

export default SwitchCfg;
