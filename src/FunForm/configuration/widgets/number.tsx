import React from "react";
import { Form, InputNumber } from "antd";
import WidegetWrap from "./widgetWrap";
const { Item: FormItem } = Form;

const NumberCfg = (props: ActivedElemnetType<FunFormWidget>) => {
  return (
    <WidegetWrap {...props}>
      <FormItem label="缺省" name="value">
        <InputNumber placeholder="设置缺省值" style={{ width: "100%" }} />
      </FormItem>
      <FormItem label="最小值" name="min">
        <InputNumber placeholder="设置最小值" style={{ width: "100%" }} />
      </FormItem>
      <FormItem label="最大值" name="max">
        <InputNumber placeholder="设置最大值" style={{ width: "100%" }} />
      </FormItem>
    </WidegetWrap>
  );
};

export default NumberCfg;
