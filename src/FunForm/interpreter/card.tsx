import React, { memo, useMemo } from "react";
import classNames from "classnames";
import { WidgetRender } from "../widgets";
import styles from "./index.module.less";

const ColunmBlock = memo(
  ({
    column,
    mode,
    labelWidth,
  }: {
    column: FunFormCardColumn;
    labelWidth: number;
    mode: "horizontal" | "vertical";
  }) => {
    return (
      <div
        key={column.uuid}
        className={styles.item}
        style={{ flex: column.colSpan }}
      >
        <div className={styles.blockDrop}>
          {column.category && (
            <WidgetRender
              {...(column.widget as FunFormWidget)}
              labelWidth={labelWidth}
              mode={mode}
              isview
            ></WidgetRender>
          )}
        </div>
      </div>
    );
  }
);

const CardInter = ({ card }: { card: FunFormCard }) => {
  const rows = useMemo(() => {
    return card.rows.filter((row) => row.columns.length);
  }, [card.rows]);
  return (
    <div
      className={classNames(styles.card, card.border === 2 && styles.borded)}
    >
      {!card.isHide && <div className={styles.card_title}>{card.title}</div>}
      <div className={styles.card_body}>
        {rows.map((row) => {
          return (
            <div className={styles.rowBlock} key={row.uuid}>
              {row.columns.map((column, g, list) => {
                let offset = 0;
                for (let i = 0; i < g; i++) {
                  offset += list[i].colSpan - 1;
                }
                return (
                  <ColunmBlock
                    column={column}
                    key={column.uuid}
                    mode={card.mode}
                    labelWidth={card.labelWidths[g + offset]}
                  ></ColunmBlock>
                );
              })}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CardInter;
