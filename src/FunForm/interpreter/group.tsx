import React, { useMemo, useState } from "react";
import { Tabs } from "antd";
import classNames from "classnames";

import Card from "./card";
import styles from "./index.module.less";

const { TabPane } = Tabs;

const GroupRender = ({
  group,
  hideTitle,
}: {
  group: FunFormGroup;
  hideTitle?: boolean;
}) => {
  return (
    <div
      className={classNames(styles.group, group.border === 2 && styles.borded)}
    >
      {!group.isHide && !hideTitle && (
        <div className={styles.group_title}>{group.title}</div>
      )}

      <div className={styles.group_body}>
        {group.cards.map((card) => {
          return <Card card={card} key={card.uuid}></Card>;
        })}
      </div>
    </div>
  );
};

export const TabGroupRender = ({ groups }: { groups: Array<FunFormGroup> }) => {
  const [activeKey, setActiveKey] = useState(groups[0].uuid);
  const list = useMemo(() => groups.filter((group) => !group.isHide), [groups]);
  return (
    <Tabs
      activeKey={activeKey}
      onChange={(key) => {
        setActiveKey(key);
      }}
    >
      {list.map((group) => {
        return (
          <TabPane key={group.uuid} tab={group.title}>
            <GroupRender group={group} hideTitle></GroupRender>
          </TabPane>
        );
      })}
    </Tabs>
  );
};

export default GroupRender;
