import { createContext } from "react";

export enum WidgetType {
  Text = "text",
  Textarea = "textarea",
  Number = "number",
  Radio = "radio",
  Date = "date",
  CheckBox = "checkbox",
  Select = "select",
  Switch = "switch",
  RichText = "richText",
  Rate = "rate",
  Address = "address",
}

export enum FunFormElement {
  Cagetroy = "cagtegory",
  Widget = "widget",
  Group = "group",
  Card = "card",
  Button = "button",
}

export enum AddressMode {
  /**国家 */
  c = "c",
  /**省市 */
  p_c = "pc",
  /**省市区县 */
  p_c_d = "pcd",
  /**省市区县详情 */
  p_c_d_d = "pcdd",
}

export const guid = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c === "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

// 这里不使用构造函数 是因为是纯json 的配置信息 如果要实例化 那么需要有 json -> class 的过程 数据量大 且繁琐没必要
export const genWidget = (category: WidgetCategory): FunFormWidget => {
  return {
    uuid: guid(),
    category,
    label: category.name,
    hidelabel: false,
    isRequired: false,
    type: FunFormElement.Widget,
    name: undefined,
  };
};

export const genColumn = (
  category?: WidgetCategory,
  widget?: FunFormWidget
): FunFormCardColumn => {
  return {
    category,
    widget,
    uuid: guid(),
    colSpan: 1,
  };
};

export const genRow = (): FunFormCardRow => ({ uuid: guid(), columns: [] });

type genButtonCfg = {
  title: string;
  isHieghtLight?: boolean;
  isSystemBtn?: boolean;
};

export const genButton = ({
  title = "按钮",
  isHieghtLight,
  isSystemBtn = false,
}: genButtonCfg): FunFormButton => ({
  uuid: guid(),
  title,
  isSystemBtn,
  isHieghtLight,
  type: FunFormElement.Button,
});

export const genCard = (title: string = "新分组"): FunFormCard => {
  const defaultRow = genRow();
  return {
    uuid: guid(),
    type: FunFormElement.Card,
    isHide: false,
    border: 1,
    labelWidths: [70, 70],
    mode: "horizontal",
    title,
    rows: [defaultRow],
  };
};

export const genGroup = (title: string = "新分区"): FunFormGroup => {
  const newCard = genCard("新分组");
  return {
    title,
    uuid: guid(),
    cards: [newCard],
    type: FunFormElement.Group,
    isHide: false,
    border: 1,
  };
};

export const getInitValues = (instance: FunFormSchema) => {
  const formSchama: Record<string, any> = {};
  const erroInfo: string[] = [];
  for (const group of instance.groups) {
    for (const card of group.cards) {
      for (const row of card.rows) {
        for (const column of row.columns) {
          if (column.widget) {
            const name = column.widget.name;
            if (!name) {
              erroInfo.push(
                `【${group.title}】->【${card.title}】-> 【${column.category?.name}】未配置对应字段`
              );
            } else {
              formSchama[name] = column.widget.value;
            }
          }
        }
      }
    }
  }
  return { formSchama, erroInfo };
};

export const genSchema = (title = "funForm"): FunFormSchema => ({
  title,
  groups: [genGroup()],
  buttonGroups: [
    genButton({
      title: "保存",
      isSystemBtn: true,
      isHieghtLight: true,
    }),
  ],
});

export default createContext<FunForm>({
  instance: genSchema(),
  updateInstance() {},
  setActivedWidgetCategory() {},
  setActivedWidget() {},
  setActivedElement() {},
});
