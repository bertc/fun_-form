import React from "react";
import { Input, Row, Select, Col, Form, Cascader } from "antd";
import pcdJson from "./pcd.json";
import countryJson from "./country.json";
import { genWidget, AddressMode } from "../core";
import ItemLayout, { WrapProps } from "./formLabel";

export const genAddressWidget = (c: WidgetCategory): AddressWidget => {
  const base = genWidget(c);
  return {
    ...base,
    name: "Address",
    label: "地址",
    placeholder: "请选择地址信息",
    allowClear: true,
    addressMode: AddressMode.p_c_d_d,
    detail: "detail",
    country: "country",
    province: "province",
    district: "district",
    city: "city",
  };
};
type PCDItem = { name: string; code: string; children?: Array<PCDItem> };

const PCJosn = pcdJson.map((item: PCDItem) => {
  const children = item.children?.map((child) => ({
    name: child.name,
    code: child.code,
    /** 省略最后一级 */
  }));
  return { name: item.name, code: item.code, children };
});

const AddressSelect = (
  props: {
    value?: string;
    onChange?: (newValue: string) => void;
  } & AddressWidget
) => {
  return (
    <React.Fragment>
      {
        {
          [AddressMode.p_c_d]: (
            <Select
              open={false}
              placeholder="请选择省市区/县"
              style={{ width: "100%" }}
            ></Select>
          ),
          [AddressMode.c]: (
            <Select
              open={false}
              placeholder="选择国家"
              style={{ width: "100%" }}
            ></Select>
          ),
          [AddressMode.p_c_d_d]: (
            <Row>
              <Col span={10}>
                <Select
                  open={false}
                  placeholder="请选择省市区/县"
                  style={{ width: "100%" }}
                ></Select>
              </Col>
              <Col span={10} offset={1}>
                <Input placeholder="输入详情地址"></Input>
              </Col>
            </Row>
          ),
          [AddressMode.p_c]: (
            <Select
              open={false}
              placeholder="选择省市"
              style={{ width: "100%" }}
            ></Select>
          ),
        }[props.addressMode]
      }
    </React.Fragment>
  );
};

const AddressWidgetRender = (props: WrapProps<AddressWidget>) => {
  const {
    isview,
    isRequired,
    addressMode,
    province,
    country,
    district,
    detail,
    city,
  } = props;
  return (
    <ItemLayout {...props}>
      {!isview && <AddressSelect {...props} />}
      {isview && (
        <Form.Item noStyle shouldUpdate>
          {({ getFieldValue, setFieldsValue }) => {
            const country_ = getFieldValue(country);
            const province_ = getFieldValue(province);
            const city_ = getFieldValue(city);
            const district_ = getFieldValue(district);
            const detail_ = getFieldValue(detail);
            const validatorPCD = () => {
              if (!isRequired) return Promise.resolve();
              if (!province_ || !city_ || !district_) {
                return Promise.reject("请选择省市区/县");
              }
              return Promise.resolve();
            };
            const validatorPC = () => {
              if (!isRequired) return Promise.resolve();
              if (!province_ || !city_) {
                return Promise.reject("请选择省市/县");
              }
              return Promise.resolve();
            };
            const validatorDetial = () => {
              if (!isRequired) return Promise.resolve();
              if (!detail_) {
                return Promise.reject("请录入详情地址信息");
              }
              return Promise.resolve();
            };
            const validatorCountry = () => {
              if (!country_) return Promise.reject("请选择国家");
              return Promise.resolve();
            };
            const provinceJosn = pcdJson.find(
              ({ code }: PCDItem) => code === province_
            );
            const cityJosn = provinceJosn?.children.find(
              ({ code }: PCDItem) => code === city_
            );
            const districtJosn = cityJosn?.children.find(
              ({ code }: PCDItem) => code === district_
            );
            const AreaSelect = (
              <Cascader
                options={addressMode === AddressMode.p_c ? PCJosn : pcdJson}
                displayRender={() => {
                  return [
                    provinceJosn?.name,
                    cityJosn?.name,
                    districtJosn?.name,
                  ]
                    .filter((x) => !!x)
                    .join("/");
                }}
                showSearch={{
                  filter: (inputValue, path) => {
                    return path.some(({ name }) => {
                      return (
                        name.toLowerCase().indexOf(inputValue.toLowerCase()) >
                        -1
                      );
                    });
                  },
                }}
                fieldNames={{
                  label: "name",
                  value: "code",
                }}
                style={{ width: "100%" }}
                placeholder={`请选择省市${
                  addressMode !== AddressMode.p_c ? "区/县" : ""
                }`}
                onChange={(_, list = []) => {
                  setFieldsValue({
                    [province]: list[0]?.code,
                    [city]: list[1]?.code,
                    [district]: list[2]?.code,
                  });
                }}
              ></Cascader>
            );
            return {
              [AddressMode.p_c_d]: (
                <Form.Item
                  name={province}
                  required={isRequired}
                  rules={[
                    {
                      type: "method",
                      validator: validatorPCD,
                    },
                  ]}
                >
                  <Form.Item noStyle name={city}>
                    <Form.Item noStyle name={district}>
                      {AreaSelect}
                    </Form.Item>
                  </Form.Item>
                </Form.Item>
              ),
              [AddressMode.c]: (
                <Form.Item
                  name={country}
                  required={isRequired}
                  rules={[
                    {
                      type: "method",
                      validator: validatorCountry,
                    },
                  ]}
                >
                  <Select
                    placeholder="选择国家"
                    showSearch
                    filterOption={(input, option) => {
                      const key = (option?.key + "").toLocaleLowerCase();
                      const value = (option?.value + "").toLocaleLowerCase();
                      if (!key || !value) return false;
                      const skey = input.toLocaleLowerCase();
                      return key.indexOf(skey) > -1 || value.indexOf(skey) > -1;
                    }}
                    style={{ width: "100%" }}
                  >
                    {Object.entries(countryJson).map(([k, v]) => {
                      return (
                        <Select.Option value={v} key={k}>
                          {v as string}
                        </Select.Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              ),
              [AddressMode.p_c_d_d]: (
                <Row>
                  <Col span={10}>
                    <Form.Item
                      name={province}
                      required={isRequired}
                      rules={[
                        {
                          type: "method",
                          validator: validatorPCD,
                        },
                      ]}
                    >
                      <Form.Item noStyle name={city}>
                        <Form.Item noStyle name={district}>
                          {AreaSelect}
                        </Form.Item>
                      </Form.Item>
                    </Form.Item>
                  </Col>
                  <Col span={10} offset={1}>
                    <Form.Item
                      name={detail}
                      required={isRequired}
                      rules={[
                        {
                          type: "method",
                          validator: validatorDetial,
                        },
                      ]}
                    >
                      <Input placeholder="输入详情地址"></Input>
                    </Form.Item>
                  </Col>
                </Row>
              ),
              [AddressMode.p_c]: (
                <Form.Item
                  name={province}
                  required={isRequired}
                  rules={[
                    {
                      type: "method",
                      validator: validatorPC,
                    },
                  ]}
                >
                  <Form.Item noStyle name={city}>
                    {AreaSelect}
                  </Form.Item>
                </Form.Item>
              ),
            }[addressMode];
          }}
        </Form.Item>
      )}
    </ItemLayout>
  );
};

export default AddressWidgetRender;
