import React from "react";
import { DatePicker, Form } from "antd";
import { genWidget } from "../core";
import ItemLayout, { WrapProps } from "./formLabel";

export const genDateWidget = (c: WidgetCategory): DateWidget => {
  const base = genWidget(c);
  return {
    ...base,
    label: "日期",
    placeholder: "请选择日期",
  };
};

const DateRender = (props: WrapProps<DateWidget>) => {
  const { placeholder, uuid, isview, name, isRequired } = props;
  return (
    <ItemLayout {...props}>
      {!isview && (
        <DatePicker
          id={uuid}
          placeholder={placeholder}
          open={false}
          style={{ width: "100%" }}
        />
      )}
      {isview && (
        <Form.Item
          name={name}
          required={isRequired}
          rules={[
            {
              required: isRequired,
              message: placeholder,
            },
          ]}
        >
          <DatePicker
            id={uuid}
            placeholder={placeholder}
            style={{ width: "100%" }}
          />
        </Form.Item>
      )}
    </ItemLayout>
  );
};

export default DateRender;
