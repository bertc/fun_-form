import React from "react";
import { Form, InputNumber } from "antd";
import { genWidget } from "../core";
import ItemLayout, { WrapProps } from "./formLabel";

export const genNumberWidget = (c: WidgetCategory): NumberWidget => {
  const base = genWidget(c);
  return {
    ...base,
    label: "数字",
    placeholder: "请输入数字",
  };
};

const NumberWidgetRender = (props: WrapProps<NumberWidget>) => {
  const { placeholder, min, max, uuid, isview, isRequired, name } = props;
  return (
    <ItemLayout {...props}>
      {!isview && (
        <InputNumber
          min={min}
          max={max}
          id={uuid}
          placeholder={placeholder}
          style={{ width: "100%" }}
        />
      )}
      {isview && (
        <Form.Item
          name={name}
          required={isRequired}
          rules={[
            {
              required: isRequired,
              message: placeholder,
            },
          ]}
        >
          <InputNumber
            min={min}
            max={max}
            id={uuid}
            placeholder={placeholder}
            style={{ width: "100%" }}
          />
        </Form.Item>
      )}
    </ItemLayout>
  );
};

export default NumberWidgetRender;
