import classNames from "classnames";
import React, { PropsWithChildren } from "react";
import styles from "./index.module.less";

export type WrapProps<T extends FunFormWidget = FunFormWidget> = T & {
  mode?: "vertical" | "horizontal";
  isview?: boolean;
};

const FormLabel = (props: PropsWithChildren<WrapProps>) => {
  const {
    labelWidth = 50,
    label = "标题",
    mode = "horizontal",
    children,
    isRequired,
    hidelabel,
  } = props;

  return (
    <div
      className={classNames(
        mode === "horizontal" && styles.horizontal,
        mode === "vertical" && styles.vertical
      )}
    >
      {!hidelabel && (
        <div
          style={{
            width: mode === "horizontal" ? labelWidth : "auto",
          }}
          className={styles.label}
        >
          {isRequired && <span className={styles.dot}>*</span>} {label}：
        </div>
      )}
      <div className={styles.content}>{children}</div>
    </div>
  );
};

export default FormLabel;
