import React, { useMemo } from "react";
import TextRender, { genTextWidget } from "./text";
import NumberRender, { genNumberWidget } from "./number";
import RadioRender, { genRadioWidget } from "./radio";
import SelectRender, { genSelectWidget } from "./select";
import DateRender, { genDateWidget } from "./date";
import CheckboxRender, { genCheckboxWidget } from "./checkbox";
import SwitchRender, { genSwitchWidget } from "./switch";
import RichTextRender, { genRichTextWidget } from "./richText";
import RateRender, { genRateWidget } from "./rate";
import AddressWidgetRender, { genAddressWidget } from "./address";
import TextareaWidgetRender, { genTextareaWidget } from "./textarea";
import { WidgetType } from "../core";
// widget 生成器
export const WidgetConstructor = (opt: WidgetCategory) =>
  ({
    [WidgetType.Text]: genTextWidget,
    [WidgetType.Number]: genNumberWidget,
    [WidgetType.Radio]: genRadioWidget,
    [WidgetType.Date]: genDateWidget,
    [WidgetType.CheckBox]: genCheckboxWidget,
    [WidgetType.Select]: genSelectWidget,
    [WidgetType.Switch]: genSwitchWidget,
    [WidgetType.RichText]: genRichTextWidget,
    [WidgetType.Rate]: genRateWidget,
    [WidgetType.Address]: genAddressWidget,
    [WidgetType.Textarea]: genTextareaWidget,
  }[opt.type](opt));

// widget 解释器
export const WidgetRender = (
  widget: FunFormWidget & {
    labelWidth: number;
    mode: "horizontal" | "vertical";
    isview?: boolean;
  }
) => {
  const type = useMemo(() => widget.category.type, [widget]);
  const Cmp = {
    [WidgetType.Text]: TextRender,
    [WidgetType.Number]: NumberRender,
    [WidgetType.Radio]: RadioRender,
    [WidgetType.Date]: DateRender,
    [WidgetType.CheckBox]: CheckboxRender,
    [WidgetType.Select]: SelectRender,
    [WidgetType.Switch]: SwitchRender,
    [WidgetType.RichText]: RichTextRender,
    [WidgetType.Rate]: RateRender,
    [WidgetType.Address]: AddressWidgetRender,
    [WidgetType.Textarea]: TextareaWidgetRender,
  }[type];
  return <Cmp {...(widget as any)} />;
};
