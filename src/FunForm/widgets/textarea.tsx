import React from "react";
import { Input, Form } from "antd";
import { genWidget } from "../core";
import ItemLayout, { WrapProps } from "./formLabel";

export const genTextareaWidget = (c: WidgetCategory): TextareaWidget => {
  const base = genWidget(c);
  return { ...base, label: "多行文本", placeholder: "请输入文本内容" };
};

const TextareaWidgetRender = (props: WrapProps<TextareaWidget>) => {
  const { isview, placeholder, maxLength, name, isRequired, showCount } = props;
  return (
    <ItemLayout {...props}>
      {!isview && (
        <Input.TextArea
          showCount={showCount}
          maxLength={maxLength}
          placeholder={placeholder}
        />
      )}
      {isview && (
        <Form.Item
          name={name}
          required={isRequired}
          rules={[
            {
              required: isRequired,
              message: placeholder,
            },
          ]}
        >
          <Input.TextArea
            showCount={showCount}
            maxLength={maxLength}
            placeholder={placeholder}
          />
        </Form.Item>
      )}
    </ItemLayout>
  );
};

export default TextareaWidgetRender;
