import React from "react";
import { Form, Rate } from "antd";
import { genWidget } from "../core";
import ItemLayout, { WrapProps } from "./formLabel";

export const genRateWidget = (c: WidgetCategory): RateWidget => {
  const base = genWidget(c);
  return {
    ...base,
    label: "评分",
    placeholder: "请选择",
    count: 5,
    value: 0,
  };
};

const RateRender = (props: WrapProps<RateWidget>) => {
  const { name, isRequired, placeholder, isview, count } = props;
  return (
    <ItemLayout {...props}>
      {!isview && <Rate count={count} value={3} disabled></Rate>}
      {isview && (
        <Form.Item
          name={name}
          required={isRequired}
          rules={[
            {
              required: isRequired,
              message: placeholder,
            },
          ]}
        >
          <Rate count={count}></Rate>
        </Form.Item>
      )}
    </ItemLayout>
  );
};

export default RateRender;
