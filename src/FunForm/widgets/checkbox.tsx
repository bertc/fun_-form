import React, { useMemo } from "react";
import { Checkbox, Form } from "antd";
import { genWidget } from "../core";
import ItemLayout, { WrapProps } from "./formLabel";

export const genCheckboxWidget = (c: WidgetCategory): CheckboxWidget => {
  const base = genWidget(c);
  return {
    ...base,
    label: "多选",
    placeholder: "请选择",
    options: [
      {
        label: "选项1",
        value: "1",
      },
      {
        label: "选项2",
        value: "2",
      },
    ],
  };
};

const CheckboxRender = (props: WrapProps<CheckboxWidget>) => {
  const { options, isRequired, isview, placeholder, name } = props;
  const hodleList = useMemo(
    () => options?.filter((x) => x.label && x.value) || [],
    [options]
  );
  return (
    <ItemLayout {...props}>
      {!isview && (
        <Checkbox.Group style={{ paddingTop: 6 }}>
          {hodleList.map((opt, i) => (
            <Checkbox value={opt.value} key={i}>
              {opt.label}
            </Checkbox>
          ))}
        </Checkbox.Group>
      )}
      {isview && (
        <Form.Item
          name={name}
          required={isRequired}
          rules={[
            {
              required: isRequired,
              message: placeholder,
            },
          ]}
        >
          <Checkbox.Group>
            {hodleList.map((opt, i) => (
              <Checkbox value={opt.value} key={i}>
                {opt.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </Form.Item>
      )}
    </ItemLayout>
  );
};

export default CheckboxRender;
